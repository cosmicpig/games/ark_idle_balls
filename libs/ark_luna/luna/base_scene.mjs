import { Utils } from "./utils.mjs"

export class Base_Scene
    extends PIXI.Container
{
    //--------------------------------------------------------------------------
    constructor()
    {
        super();
        Utils.add_luna_unique_object_id(this);
    } // scene

    //--------------------------------------------------------------------------
    update(dt)
    {
        // Do nothing...
    }

    //--------------------------------------------------------------------------
    on_load()
    {
        // Do nothing...
        console.log("[on_load]", this.constructor.name, this._luna_object_id);
    }

    //--------------------------------------------------------------------------
    on_unload()
    {
        // Do nothing...
        console.log("[on_unload]", this.constructor.name, this._luna_object_id);
    }

    //--------------------------------------------------------------------------
    on_enter()
    {
        // Do nothing...
        console.log("[on_enter]", this.constructor.name, this._luna_object_id);
    }

    //--------------------------------------------------------------------------
    on_exit()
    {
        // Do nothing...
        console.log("[on_exit]", this.constructor.name, this._luna_object_id);
    }

}; // class BaseScene
