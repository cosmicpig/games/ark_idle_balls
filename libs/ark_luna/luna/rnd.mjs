export class Rnd
{
    static next_int(min, max)
    {
        if(min === undefined && max == undefined) {
            min = Number.MIN_SAFE_INTEGER;
            max = Number.MAX_SAFE_INTEGER;
        } else if(max == undefined) {
            max = Number.MAX_SAFE_INTEGER;
        }

        const value = min + Math.random() * (max - min);
        return Math.trunc(value);
    }

    static next(min, max) {
        return min + Math.random() * (max - min);
    }
}
