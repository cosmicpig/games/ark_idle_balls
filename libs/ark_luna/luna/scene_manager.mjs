import {Arr} from "./arr.mjs"
import {App} from "./app.mjs"

export class Scene_Manager
{
    static _registeres_scenes = null;
    static _default_scene     = null;
    static _splash_scene      = null;
    static _already_splashed  = false;

    static curr_scene  = null;
    static scene_stack = null


    //--------------------------------------------------------------------------
    static init()
    {
        Scene_Manager._registeres_scenes = new Map();
        Scene_Manager.scene_stack        = [];
    }

    //--------------------------------------------------------------------------
    static register_scene(...scenes_to_register)
    {
        for(let i = 0; i < scenes_to_register.length; ++i) {
            const scene = scenes_to_register[i];
            const clean_name = scene.name.toLowerCase();
            Scene_Manager._registeres_scenes.set(clean_name, scene);
        }
    }

    //--------------------------------------------------------------------------
    static set_splash_scene(splash_scene)
    {
        Scene_Manager._splash_scene = splash_scene;
    }

    //--------------------------------------------------------------------------
    static set_default_scene(default_scene)
    {
        Scene_Manager._default_scene = default_scene;
    }

    //--------------------------------------------------------------------------
    static run_with(params)
    {
        if(!Scene_Manager._already_splashed) {
            Scene_Manager._already_splashed = true;

            const scene = new Scene_Manager._splash_scene();
            scene.on_scene_manager_splash_has_completed = ()=>{
                Scene_Manager.run_with(params);
            };
            Scene_Manager.set_scene(scene);

            return;
        }

        const scene_name     = params.get("scene");
        let   scene_to_start = Scene_Manager._default_scene;
        if(scene_name) {
            scene_to_start = Scene_Manager._registeres_scenes.get(scene_name);
            if(!scene_to_start) {
                console.log("Scene is not registered", scene_name);
                debugger;
            }
        }

        Scene_Manager.set_scene(new scene_to_start());

    }

    //--------------------------------------------------------------------------
    static push_scene(scene)
    {
        if(typeof scene === "string" || scene instanceof String) {
            const scene_name  = scene.toLowerCase();
            const scene_class = Scene_Manager._registeres_scenes.get(scene_name);
            scene = new scene_class();
        }

        // Pushing the same scene....
        if(Scene_Manager.curr_scene != null &&
           Scene_Manager.curr_scene._luna_object_id == scene._luna_object_id)
        {
            debugger;
            return;
        }

        if(Scene_Manager.curr_scene) {
            Scene_Manager.curr_scene.on_exit();
            Scene_Manager.curr_scene.parent.removeChild(Scene_Manager.curr_scene);
        }

        Scene_Manager.curr_scene = scene;
        Scene_Manager.curr_scene.on_load();

        Scene_Manager.scene_stack.push(scene);
        App.get_pixi_instance().stage.addChild(Scene_Manager.curr_scene);

        Scene_Manager.curr_scene.on_enter();
    } // Push Scene

    //--------------------------------------------------------------------------
    static set_scene(scene)
    {
        while(Scene_Manager.scene_stack.length != 0) {
            Scene_Manager.pop_scene();
        }

        Scene_Manager.curr_scene = null;
        Scene_Manager.push_scene(scene);
    }

    //--------------------------------------------------------------------------
    static pop_scene()
    {
        const scene = Arr.get_back(Scene_Manager.scene_stack);
        scene.on_exit();

        if(scene.parent != null) {
            scene.parent.removeChild(scene);
        }

        scene.on_unload();
        Arr.remove_last(Scene_Manager.scene_stack);

        const prev_scene = Arr.get_back(Scene_Manager.scene_stack);
        if(prev_scene) {
            App.stage.addChild(prev_scene);
            prev_scene.on_enter();
            Scene_Manager.curr_scene = prev_scene;
        }
    }

    //--------------------------------------------------------------------------
    static update(dt)
    {
        Scene_Manager.curr_scene.update(dt);
    }
};
