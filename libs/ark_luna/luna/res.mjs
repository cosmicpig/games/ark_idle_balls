import {Utils} from "./utils.mjs";
import {Str}   from "./str.mjs";

class Loader
    extends PIXI.Loader
{
    constructor()
    {
        super();

        this._is_pixi_finished  = false;
        this._is_fonts_finished = false;

        this._pixi_filenames  = [];
        this._font_filenames  = [];

        this._pixi_load_count = 0;
        this._font_load_count = 0;
    }

    set_resources_to_load(resources_to_load)
    {
        /// @XXX(stmatt): Passing multiple times and removing things from the array...
        /// Would be much faster to iterate just once and create an array that we
        //  separate the things in one gone - 7/27/2021, 2:11:28 AM
        let expanded_args = [];
        for(let i = 0; i < resources_to_load.length; ++i) {
            const curr_arg = resources_to_load[i];
            if(Array.isArray(curr_arg)) {
                expanded_args = expanded_args.concat(curr_arg);
            } else {
                expanded_args.push(curr_arg);
            }
        }

        let pixi_names = [];
        let font_names = [];
        for(let i = 0; i < expanded_args.length; ++i) {
            const name = expanded_args[i];
            if(RES.is_resource_loaded(name)) {
                console.log("Resource is already loaded:", name);
                continue;
            }

            const ext = Utils.find_extension(name);
            if(ext == ".ttf") {  // @XXX(stdmatt): Add more extensions...7/27/2021, 2:16:54 AM
                font_names.push(name);
            } else {
                pixi_names.push(name);
            }
        }

        this._font_filenames = font_names;
        this._pixi_filenames = pixi_names;

        this._font_load_count = this._font_filenames.length;
        this._pixi_load_count = this._pixi_filenames.length;
    }

    set_callbacks(info)
    {
        this.onProgress.add((_, r)=>{
            if(info.on_progress) {
                info.on_progress(r);
            }
        });

        this.onError.add((_, r)=>{
            --this._pixi_load_count;
            if(info.on_error) {
                info.on_error(r);
            }
        });

        this.onLoad.add((_, r)=>{
            --this._pixi_load_count;
            if(info.on_load) {
                info.on_load(r);
            }
            });
    }

    load_resources(on_finish_callback)
    {
        this._is_pixi_finished  = false;
        this._is_fonts_finished = false;

        // Load the resources that Pixi can handle.
        this.add(this._pixi_filenames).load(()=>{
            this._check_completion(on_finish_callback);
        });

        // Load the fonts.
        for(let i = 0; i < this._font_filenames.length; ++i) {
            const filename = this._font_filenames[i];
            const basename = Utils.basename        (filename);
            const name     = Utils.remove_extension(basename);
            const url      = Str.concat("url(",filename,")");

            const font = new FontFace(name, url);
            font.load()
                .then((loaded_face)=>{
                    document.fonts.add(loaded_face);
                    document.body.style.fontFamily = Str.concat(name, ", ", document.body.style.fontFamily);
                    this.resources[name] = loaded_face;
                    --this._font_load_count;
                    this._check_completion(on_finish_callback);
                })
                .catch((error)=>{
                    --this._font_load_count;
                    this._check_completion(on_finish_callback);
                });
        }
    }

    _check_completion(on_finish_callback)
    {
        if(this._font_load_count <= 0 && this._pixi_load_count <= 0) {
            on_finish_callback();
            this.resources = {}; // @XXX(stdmatt): Find a way to clean without generate garbage - 7/27/2021, 2:39:07 AM
        }
    }
};

export class RES
{
    //------------------------------------------------------------------------------
    static _loaders_pool     = null;
    static _loaded_resources = null;

    static init()
    {
        RES._loaded_resources = new Map  ();
        RES._loaders_pool     = new Array();
    }

    static load_resources_with_info(info)
    {
        // @XXX(stdmatt): We need to make sure that the loader object outlives the loading process... - Jul 26, 21
        const loader = RES._get_available_loader();
        loader.set_resources_to_load(info.resources_to_load);
        loader.set_callbacks        (info);

        return new Promise((resolve, reject) => {
            loader.load_resources(()=>{
                // @notice(stdmatt): Add all loaded resources from this loader to RES.
                for(const key in loader.resources) {
                    RES._loaded_resources.set(key, loader.resources[key]);
                }
                if(info.on_complete) {
                    info.on_complete();
                }
                resolve();
            });
        });
    }

    static _get_available_loader()
    {
        for(let i = 0; i < this._loaders_pool.length; ++i) {
            const loader = this._loaders_pool[i];
            if(loader._is_finished) {
                return loader;
            }
        }

        const loader = new Loader();
        this._loaders_pool.push(loader);
        return loader;
    }

    static is_resource_loaded(name)
    {
        const v = RES._loaded_resources.get(name);
        if(v) { return true; }
        return false;
    }


    static get_texture(texture_name)
    {
        const resource = RES._loaded_resources.get(texture_name);
        if(!resource) {
            console.log("Can't find texture - Name:(", name, ")");
            debugger;
        }

        return resource.texture;
    }

    static get_textures(...texture_names)
    {
        const arr = [];
        for(let i = 0; i < texture_names.length; ++i) {
            const texture_name = texture_names[i];
            const texture      = RES.get_texture(texture_name);
            arr.push(texture);
        }
        return arr;
    }

    //--------------------------------------------------------------------------
    static get_data(data_name)
    {
        const resource = RES._loaded_resources.get(data_name);
        if(!resource) {
            console.log("Can't find data Name:(", name, ")");
            debugger;
        }

        return resource.data;
    }

    //--------------------------------------------------------------------------
    static create_sprite_with_white_texture(width, height)
    {
        const sprite = new PIXI.Sprite(PIXI.Texture.WHITE);
        if(width && height) {
            sprite.width  = width;
            sprite.height = height;
        }

        return sprite;
    }

    static create_sprite_with_texture(texture)
    {
        const sprite = new PIXI.Sprite(texture);
        return sprite;
    }

    static create_sprite_with_texture_name(texture_name)
    {
        const texture = RES.get_texture(texture_name);
        const sprite  = new PIXI.Sprite(texture);
        return sprite;
    }
}
