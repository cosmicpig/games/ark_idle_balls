import {App} from "./app.mjs"
import {Utils} from "./utils.mjs";


export class Input_Manager
{
    static EVENT_ON_CLICK   = 0;
    static EVENT_ON_PRESS   = 0;
    static EVENT_ON_RELEASE = 0;

    //--------------------------------------------------------------------------
    static Keyboard         = null;

    static Mouse            = null;
    static is_mouse_enabled = false;

    //--------------------------------------------------------------------------
    static init()
    {
        Input_Manager.Mouse = new _Mouse();
    }

    //--------------------------------------------------------------------------
    static install_basic_mouse_handlers(html_element)
    {
        html_element = html_element || App.get_pixi_instance().view;
        Input_Manager.is_mouse_enabled = true;

        //
        // Move
        html_element.addEventListener("mousemove", function(e) {
            const r = html_element.getBoundingClientRect();
            Input_Manager.Mouse.x = (e.clientX - r.left) / (r.right  - r.left) * html_element.width;
            Input_Manager.Mouse.y = (e.clientY - r.top ) / (r.bottom - r.top ) * html_element.height;
        }, false);

        //
        // Left Mouse Click
        html_element.addEventListener("click", event => {
            Input_Manager.Mouse.left_button = true;
            const handlers = Input_Manager.Mouse._on_click_handlers;
            for (let [key, value] of handlers) {
                value();
            }
        });

        //
        // Right Mouse Click
        html_element.addEventListener('contextmenu', function(ev) {
            ev.preventDefault();
            Input_Manager.Mouse.right_button = true;
        }, false);

        //
        // Mouse Down
        html_element.addEventListener("mousedown", event => {
            if(event) {
                Input_Manager.Mouse.left_button = true;
            } else if(event) {
                Input_Manager.Mouse.middle_button = true;
            } else {
                Input_Manager.Mouse.right_button = true;
            }
        });

        //
        // Mouse Up
        html_element.addEventListener("mouseup", event => {
            if(event) {
                Input_Manager.Mouse.left_button = false;
            } else if(event) {
                Input_Manager.Mouse.midle_button = false;
            } else {
                Input_Manager.Mouse.right_button = false;
            }
         });

         //
         // Mouse Whell
         html_element.addEventListener("wheel", event => {
            Input_Manager.Mouse.wheel.x += event.wheelDeltaX;
            Input_Manager.Mouse.wheel.y += event.wheelDeltaY;
         });
    }
};


//
//
//
class _Mouse
{
    constructor()
    {
        this.x = 0;
        this.y = 0;
        this.left_button   =  false;
        this.right_button  =  false;
        this.middle_button =  false;
        this.wheel         = { x: 0, y: 0 };

        this._on_click_handlers   = new Map();
        this._on_press_handlers   = new Map();
        this._on_release_handlers = new Map();
    }

    add_handler(event_type, handler)
    {
        const obj_id = Utils.get_luna_unique_object_id(handler);
        if(event_type == Input_Manager.EVENT_ON_CLICK) {
            if(this._on_click_handlers.get(obj_id) != null) {
                console.log("Already has a handler with this id", obj_id);
                debugger;
            }
            this._on_click_handlers.set(obj_id, handler);
        }
    }
}
