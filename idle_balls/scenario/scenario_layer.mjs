import { luna }           from "../../libs/ark_luna/luna/luna.mjs";
import { Game_Utils }     from "../game_utils.mjs";
import { Sky_Background } from "./sky_background.mjs";
import { Ground }         from "./ground.mjs";
import { Scenary }        from "./scenary.mjs";

//
//
//
export class Scenario_Layer
    extends PIXI.Container
{
    //--------------------------------------------------------------------------
    constructor()
    {
        super();

        this.sky     = new Sky_Background();
        this.ground  = luna.Layout.create_fixed_size_container(100, 100); //new Ground (Game_Utils.GROUND_OPTIONS);
        this.scenary = new Scenary(Game_Utils.SCENARY_OPTIONS);

        luna.Layout.set_anchor_to(0.5, 1, this.ground, this.scenary);
        luna.Layout.set_y_relative_to_design_size(1, this.ground);
        //luna.Layout.set_center_relative_to_design_size(0.5, 1, this.ground);

        const ground_top = luna.Layout.get_top_of(this.ground);
        this.scenary.x = this.ground.x;
        this.scenary.y = ground_top;

        luna.Layout.add_to_parent(this, this.sky, this.ground, this.scenary);
    } // CTOR


    //--------------------------------------------------------------------------
    reparent_layer(new_parent)
    {
        luna.Layout.remove_from_parent(this);
        luna.Layout.add_to_parent(new_parent, this);
    }

    //--------------------------------------------------------------------------
    update(dt)
    {
        this.sky    .update(dt);
        // this.ground .update(dt);
        this.scenary.update(dt);
    }
}

//
//
//
let _s_scenario_layer = null;
export function Get_Scenario_Layer(new_parent)
{
    if(!_s_scenario_layer) {
        _s_scenario_layer = new Scenario_Layer();
    }

    _s_scenario_layer.reparent_layer(new_parent);
    return _s_scenario_layer;
}
