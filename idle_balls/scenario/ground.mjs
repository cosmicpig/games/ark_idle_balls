import { luna }           from "../../libs/ark_luna/luna/luna.mjs";

//
//
//
export class Ground
    extends PIXI.Container
{
    constructor(options)
    {
        super();

        this.options        = options;

        this.textures       = luna.RES.get_textures(...options.ground_texture_names);
        this.texture_width  = this.textures[0].width;
        this.texture_height = this.textures[0].height;
        this.ground_width   = luna.Utils.get_value_or_default(this.options.width, luna.Layout.DESIGN_WIDTH)
        this.ground_height  = luna.Utils.get_value_or_default(this.options.width, luna.Layout.DESIGN_HEIGHT * 0.5);

        this.ground_layer = this._generate_ground_layer();
        this.addChild(this.ground_layer);
    }

    update(dt)
    {

    }

    _generate_ground_layer()
    {
        const container = luna.Layout.create_container();
        const rows = Math.trunc(this.ground_height / this.texture_height);
        const cols = Math.trunc(this.ground_width  / this.texture_width);

        console.log(cols, rows);

        for(let i = 0; i < rows; ++i) {
            for(let j = 0; j < cols; ++j) {
                let ii = null;
                let jj = null;
                if(i == 0) {
                    ii = 0;
                } else if(i == rows -1) {
                    ii = rows - 1;
                } else {
                    ii = 1;
                }

                if(j == 0) {
                    jj = 0;
                } else if(j == cols-1) {
                    jj = cols - 1;
                } else {
                    jj = 1;
                }

                const texture_index = (ii * cols) + jj;
                const texture = this.textures[0];
                const sprite  = luna.RES.create_sprite_with_texture(texture);
                const x = (j * this.texture_width);
                const y = (i * this.texture_height);

                sprite.x = x;
                sprite.y = y;

                container.addChild(sprite);
            }
        }

        return container;
    }
}
