import { luna }        from "../../libs/ark_luna/luna/luna.mjs";
import { Game_Utils } from "../game_utils.mjs";

//
// Score Particles
//
export class Score_Particles
    extends PIXI.Container
{
    //--------------------------------------------------------------------------
    constructor()
    {
        super();
        // @todo(stdmatt): Maybe can we cache it... 8/4/2021, 2:23:53 AM
        const data     = luna.RES.get_data    (   Game_Utils.SCORE_TEXT_PARTICLE_DATA_NAME);
        const textures = luna.RES.get_textures(...Game_Utils.SCORE_TEXT_PARTICLE_TEXTURES);

        this.emitter = new PIXI.particles.Emitter(this, textures, data);
        this.emitter.emit = false;
    }

    //--------------------------------------------------------------------------
    update(dt)
    {
        this.emitter.update(dt);
    }

    //--------------------------------------------------------------------------
    play()
    {
        this.emitter.playOnce(()=>{
            this.emitter.emit = false;
        });
    }
}
