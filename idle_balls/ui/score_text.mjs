import { luna }           from "../../libs/ark_luna/luna/luna.mjs";
import { Game_Utils }     from "../game_utils.mjs";

//
// Score_Text
//
export class Score_Text
    extends PIXI.Container
{
    //--------------------------------------------------------------------------
    constructor(value, digits_count)
    {
        super()

        this.digits            = [];
        this.digits_count      = digits_count;
        this.curr_value        = Game_Utils.fill_digits(value, digits_count);
        this.tween             = null;
        this.animation_indexes = [];

        for(let i = 0; i < this.curr_value.length; ++i) {
            const digit_value = this.curr_value[i];
            const digit_text  = new luna.Base_TTFText(Game_Utils.BIG_FONT_INFO, digit_value);
            this.digits.push(digit_text);
        }


        luna.Layout.add_to_parent       (this, ...this.digits);
        luna.Layout.set_anchor_to_center(      ...this.digits);

        luna.Layout.set_left_of(0, this.digits[0]);
        luna.Layout.set_y_relative_to_parent(0.5,  ...this.digits);
        luna.Layout.stack_positions_h       (0,    ...this.digits);
    }

    //--------------------------------------------------------------------------
    set_value_animated(value, half_way_callback)
    {
        // Select the digits that has changed and will be animated.
        this.animation_indexes = [];

        const value_str = Game_Utils.fill_digits(value, this.digits_count);
        for(let i = 0; i < value_str.length; ++i) {
            if(value_str[i] == this.curr_value[i]) {
                continue;
            }
            this.animation_indexes.push(i);
        }

        this.curr_value = value_str;

        // Animate it...
        // @notice(stdmatt): Would be better to actually animate all digits
        // with just one tween and iterate inside, but right now I don't quite
        // wanna debug everything... Life is too fucking short to save a couple
        // of nanoseconds in a game of this size....
        // I gonna just take a step back, enjoy a joint and accepts that battles
        // that I'm willing to take!!
        // 8/3/2021, 8:27:01 AM
        //    Watching the CPI da Panemia in youtube while programming it.
        //    It's very very close that I came to kyiv, and as today I'm so
        //    fucking happy. Happy for the experience that I'm having today...
        //    I just wish to have the opportunity to get some of Portugal  experience
        //    again - mostly the innocense of the discovery!
        //    HAPPY!
        for(let i = 0; i < this.animation_indexes.length; ++i) {
            const index      = this.animation_indexes[i];
            const digit_text = this.digits[index];
            const duration   = Game_Utils.BUBBLE_ANIMATION_DURATION
            luna.Tween.create(duration, Game_Utils.ANIM_TAG_SCORE_BUBBLE)
                .from({s: 1})
                .to  ({s: 0})
                .yoyo(true)
                .repeat(1)
                .easing(Game_Utils.BUBBLE_ANIMATION_EASING)
                .on_update((value)=>{
                    digit_text.scale.set(value.s);
                })
                .on_repeat(()=>{
                    digit_text.text = this.curr_value[index];
                    if(half_way_callback) {
                        half_way_callback(this.animation_indexes.length);
                    }
                })
                .start();
        }
    }
}
