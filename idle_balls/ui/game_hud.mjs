import { luna }            from "../../libs/ark_luna/luna/luna.mjs";
import { Game_Utils }      from "../game_utils.mjs";
import { Score_Particles } from "./score_particles.mjs";
import { Score_Text }      from "./score_text.mjs";

//
// Game_Hud
//
export class Game_Hud
    extends PIXI.Container
{
    //--------------------------------------------------------------------------
    constructor()
    {
        super();

        this.level_text = null;
        this.score_text = null;
        this.particles_container = null;
        this._particles = [];

        //
        const small_font = Game_Utils.SMALL_FONT_INFO;
        const level_str = luna.Str.concat("level", " ", 1);

        this.level_text          = new luna.Base_TTFText(small_font, level_str, Game_Utils.TEXT_COLOR_YELLOW);
        this.score_text          = new Score_Text       ("0", Game_Utils.SCORE_DIGITS_COUNT);
        this.particles_container = luna.Layout.create_fixed_size_container(this.score_text.width, this.score_text.height);

        //
        luna.Layout.add_to_parent(this, this.particles_container, this.level_text, this.score_text);

        luna.Layout.set_x_relative_to_parent(0.5, this.level_text, this.score_text);
        luna.Layout.stack_positions_v       (0,   this.level_text, this.score_text);

        const anchor = luna.Layout.get_anchor_from(this.score_text);
        luna.Layout.set_anchor_to(anchor.x, anchor.y, this.particles_container);
        this.particles_container.x = this.score_text.x;
        this.particles_container.y = this.score_text.y;
    }

    //--------------------------------------------------------------------------
    update(dt)
    {
        for(let i = 0; i < this._particles.length; ++i) {
            const particle = this._particles[i];
            particle.update(dt);
        }
    }

    //--------------------------------------------------------------------------
    update_score_value(points)
    {
        this.score_text.set_value_animated(points, (count)=>{
            this._ensure_particles(count);
            for(let i = 0; i < count; ++i) {
                const particle = this._particles[i];
                const x        = luna.Rnd.next(0.2, 0.8);
                luna.Layout.set_x_relative_to_parent(x, particle);
                particle.play();
            }
        });
    }

    //--------------------------------------------------------------------------
    _ensure_particles(count)
    {
        while(count > this._particles.length) {
            const particle = new Score_Particles();

            luna.Layout.set_anchor_to_center(particle);
            luna.Layout.add_to_parent(this.particles_container, particle);
            luna.Layout.set_y_relative_to_parent(0.5, particle);

            this._particles.push(particle);
        }
    }
}
