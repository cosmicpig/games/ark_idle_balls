import { luna } from "../../../libs/ark_luna/luna/luna.mjs"
import { Game_Utils } from "../game_utils.mjs";



class Widget
    extends luna.UI.Fixed_Size_Container
{
    constructor(color)
    {
        super(100, 100, color);

        const a = new luna.UI.Fixed_Size_Container(10, 10, 0xFF0000);
        a.x = this.width * 0.5 - a.width * 0.5;
        a.y = this.height * 0.5 - a.height * 0.5;
        this.addChild(a);
    }
};




export class Layout_Test_Scene
    extends luna.Base_Scene
{

    //--------------------------------------------------------------------------
    constructor()
    {
        super();
        this.steps = [];
        this.container = null;


        luna.App.set_clear_color(luna.Utils.COLOR_WHITE);

        const _L = luna.Layout;
        this._test((container)=> {
            let widget = new Widget(0xFF00FF);
            container.addChild(widget);
            _L.set_center_relative_to_design_size(0.5, 0.5, widget);

            for(let i = 0; i < 3; ++i) {
                widget = new Widget(0x00FFF0);
                container.addChild(widget);
                let ax = luna.Rnd.next(0, 1);
                let ay = luna.Rnd.next(0, 1);
                _L.set_anchor_to(ax, ay, widget);
                _L.set_x_relative_to_design_size(0.5, widget);
                _L.stack_objects_vertically(0, ...container.children);
            }
        });


        this._test((container)=> {
            let widget = new Widget(0xFF00FF);
            _L.set_center_relative_to_design_size(0.5, 0.5, widget);
            container.addChild(widget);

            widget = new Widget(0x00FF00);
            _L.set_anchor_to(1, 1, widget);
            _L.set_center_relative_to_design_size(0.5, 0.5, widget);
            container.addChild(widget);
        });


        this._test((container)=> {
            const widget = new Widget();
            _L.set_center_relative_to_design_size(0.0, 0.5, widget);
            container.addChild(widget);
        });

        this._test((container)=> {
            const widget = new Widget();
            _L.set_center_relative_to_design_size(1, 0.5, widget);
            container.addChild(widget);
        });

        this._test((container)=> {
            const widget = new Widget();
            _L.set_y_relative_to_design_size(1, widget);
            container.addChild(widget);
        });

        this.interactive = true; // !pixi
        this.buttonMode  = true; // !pixi

        this.on("pointerdown", ()=> {
            const l = this.steps.length;
            if(l > 0) {
                if(this.container != null) {
                    luna.Layout.remove_from_parent(this.container);
                }

                const test = luna.Arr.get_front(this.steps);
                test();
                luna.Arr.remove_at(this.steps, 0);
            }
        });

        const test = luna.Arr.get_front(this.steps);
        test();
        luna.Arr.remove_at(this.steps, 0);
    }

    update(dt)
    {

    }

    _test(callback) {
        this.steps.push(()=>{
            const a = new PIXI.Container();
            this.addChild(a);
            this.container = a;
            callback(a);
        });
    }

}
