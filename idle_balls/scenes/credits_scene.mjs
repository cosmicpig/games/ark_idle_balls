import { luna }           from "../../libs/ark_luna/luna/luna.mjs";
import { Animated_Scene } from "./animated_scene.mjs";
import { Game_Utils }     from "../game_utils.mjs";

export class Credits_Scene
    extends Animated_Scene
{
    //--------------------------------------------------------------------------
    constructor()
    {
        super();

        // Button
        const back_button = Game_Utils.create_back_button(()=>{ this.run_on_exit("Menu_Scene"); });
        luna.Layout.add_to_parent(this, back_button);

        //
        // Msgs
        this._start_animation();
    } // CTOR

    //--------------------------------------------------------------------------
    _start_animation()
    {
        const big_font     = Game_Utils.MEDIUM_FONT_INFO;
        const small_font   = Game_Utils.SMALL_FONT_INFO;
        const header_color = Game_Utils.TEXT_COLOR_WHITE_LIGHT;
        const sub_color    = Game_Utils.TEXT_COLOR_YELLOW;
        const section_gap  = 40;

        const msgs = [
            [
                { text: "This little game", font: big_font, gap: 0, color: header_color },
                { text: "was made with",    font: big_font, gap: 0, color: header_color },
                { text: "a lot of love!",   font: big_font, gap: 0, color: header_color },

            ],

            [
                { text: "Dedicated to all",         font: big_font,   gap: 0,  color: header_color },
                { text: "people that suffered",     font: big_font,   gap: 0,  color: header_color },
                { text: "from covid-19",            font: big_font,   gap: 10, color: header_color },
                { text: "Feel strong, stay safe!",  font: small_font, gap: 0,  color: sub_color    },
            ],

            [
                { text: "A big thanks to:", font: small_font, gap: 10, color: Game_Utils.TEXT_COLOR_ORANGE},

                { text: "Maezinha e Pingo",              font: big_font,   gap: 0, color: header_color },
                { text: "Saudade nunca se vai, apenas",  font: small_font, gap: 0, color: sub_color    },
                { text: "aprendemos a conviver com ela", font: small_font, gap: 0, color: sub_color    },
            ],

            [
                { text: "STDMATT MMXXI",          font: big_font  , gap: 0, color: Game_Utils.TEXT_COLOR_WHITE_DARK},
                { text: "gplv3 - hack, share it", font: small_font, gap: section_gap, color: Game_Utils.TEXT_COLOR_WHITE_DARK}
            ],

            [
                { text: "Check here info about licenses!!", font: small_font, color: Game_Utils.TEXT_COLOR_BLUE, is_link: true},
            ],
        ];

        const labels_container = luna.Layout.create_container();
        let y = 0;
        let curr_font = small_font;
        for(let i = 0; i < msgs.length; ++i) {
            for(let j = 0; j < msgs[i].length; ++j) {
                const item   = msgs[i][j];
                const msg    = item.text;
                let   _gap   = 2;
                let   _color = 0xFFFFFF;

                if(item.font) {
                    curr_font = item.font;
                }
                if(item.gap) {
                   _gap += item.gap;
                }
                if(item.color) {
                    _color = item.color;
                }

                const text = new luna.Base_TTFText(curr_font, msg, _color);
                text.position.y = y;
                text.tint       = _color;

                y += (text.height + _gap);
                luna.Layout.add_to_parent(labels_container, text);

                // This is the copy right button
                if(item.is_link) {
                    text.interactive = true;
                    text.buttonMode  = true;
                    text.on("pointerdown", ()=>{
                        const win = window.open("./thanks_to.html", '_blank');
                        win.focus();
                    });
                }
            }
            y += section_gap;
        }

        luna.Layout.set_x_relative_to_parent(0.5, ...labels_container.children);
        luna.Layout.set_center_relative_to_design_size (0.5, 0.5, labels_container);
        luna.Layout.add_to_parent(this, labels_container);
    }
} // class CreditsScene
