import { luna }               from "../../libs/ark_luna/luna/luna.mjs";
import { Game_Utils }         from "../game_utils.mjs";
import { Get_Scenario_Layer } from "../scenario/scenario_layer.mjs";
//
// Constants
//
// @TODO(stdmatt); Move to game utils 8/2/2021, 8:20:44 AM
const ANIMATED_SCENE_CLOSE_DOWN_ANIMATION_DURATION = 0.8 * Game_Utils.ANIMATION_SPEED_MULTIPLIER;
const ANIMATED_SCENE_OPEN_UP_ANIMATION_DURATION    = 0.8 * Game_Utils.ANIMATION_SPEED_MULTIPLIER;

const ANIMATED_SCENE_CLOSE_DOWN_ANIMATION_EASING = luna.Tween.Easing.Exponential.In;
const ANIMATED_SCENE_OPEN_UP_ANIMATION_EASING    = luna.Tween.Easing.Exponential.Out;

const ANIMATED_SCENE_SHAPES = [
    RESOURCES_TEXTURES_FADE_SHAPE_5
];


//
// Animated Scene
//
export class Animated_Scene
    extends luna.Base_Scene
{
    //--------------------------------------------------------------------------
    constructor()
    {
        super();
        //
        luna.Input_Manager.is_mouse_enabled = false;

        //
        this.scenario_layer = Get_Scenario_Layer(this);

        this.is_doing_fade_animation = false;
        this.sortableChildren        = true; // !pixi
    } // CTOR

    //--------------------------------------------------------------------------
    update(dt)
    {
        this.scenario_layer.update(dt);
    }

    //--------------------------------------------------------------------------
    on_enter()
    {
        super.on_enter();
        this._create_animation_with_callback(true, ()=>{
            this.on_finished_enter_animation();
            luna.Input_Manager.is_mouse_enabled = true;
        })
    } // OnEnter

    //--------------------------------------------------------------------------
    run_on_exit(scene)
    {
        super.on_exit();
        luna.Input_Manager.is_mouse_enabled = false;
        this._create_animation_with_callback(false, ()=>{
            luna.Scene_Manager.set_scene(scene);
        })
    } // RunOnExit

    //--------------------------------------------------------------------------
    on_finished_enter_animation()
    {
        // Should be overridden...
        // debugger;
    } // OnFinishedEnterAnimation

    //--------------------------------------------------------------------------
    _create_animation_with_callback(is_opening_up, callback)
    {
        this.is_doing_fade_animation = true;

        const fade_shape = luna.Arr.random_element(ANIMATED_SCENE_SHAPES);
        this.focus = luna.RES.create_sprite_with_texture_name(fade_shape);

        // @XXX(stdmatt): This value is to make the thing goes TOTALLY out of
        // the screen, otherwise it keeps a little bit inside...
        const XXX_MAGIC = 1.15;

        const focus_radius  = this.focus.height * (luna.Layout.DESIGN_HEIGHT / this.focus.height) * XXX_MAGIC;
        const curr_radius   = (is_opening_up) ? 0 : focus_radius;
        const target_radius = (is_opening_up) ? focus_radius : 0;

        this.focus.width  = curr_radius;
        this.focus.height = curr_radius;
        luna.Layout.set_anchor_to_center(this.focus);
        luna.Layout.set_center_relative_to_design_size (0.5, 0.5, this.focus);

        this.mask = this.focus;
        this.parent.addChild(this.focus);

        const anim_time = (is_opening_up)
            ? ANIMATED_SCENE_OPEN_UP_ANIMATION_DURATION
            : ANIMATED_SCENE_CLOSE_DOWN_ANIMATION_DURATION;

        const anim_ease = (is_opening_up)
            ? ANIMATED_SCENE_OPEN_UP_ANIMATION_EASING
            : ANIMATED_SCENE_CLOSE_DOWN_ANIMATION_EASING;

        luna.Tween.create(anim_time)
            .from({r: curr_radius  })
            .to  ({r: target_radius})
            .easing(anim_ease)
            .on_update((value)=>{
                if(value.r < 0) {
                    value.r = 0;
                }
                this.focus.width  = value.r;
                this.focus.height = value.r;
            })
            .on_complete(()=>{
                luna.Layout.remove_from_parent(this.focus);
                this.mask                    = null;
                this.is_doing_fade_animation = false;

                callback();
            })
            .start();
    } // _CreateAnimationWithCallback
} // AnimatedScene
