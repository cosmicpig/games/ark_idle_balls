//
// Menu Scene
//
import {Game_Physics, Physics_Ball, Physics_Brick} from "../game_physics.mjs";
import {Game_Utils} from "../game_utils.mjs";
import {luna} from "../../libs/ark_luna/luna/luna.mjs";

export class Menu_Scene
    extends luna.Base_Scene
{
    //--------------------------------------------------------------------------
    constructor()
    {
        super();
        this.ball = new Physics_Ball(
            Game_Utils.DESIGN_WIDTH * 0.5,
            Game_Utils.DESIGN_HEIGHT * 0.5, 20
        );

        this.physics_objs = [];

        luna.Input_Manager.Mouse.add_handler(luna.Input_Manager.EVENT_ON_CLICK, ()=> {
            const mouse = luna.Input_Manager.Mouse;
            const ball = new Physics_Ball(
                mouse.x, mouse.y, Game_Utils.BALL_RADIUS
            )

            this.physics_objs.push(ball);
            luna.Layout.add_to_parent(this, ball);
        });


        const box = new Physics_Brick(200, 400);

        this.physics_objs.push(box);
        luna.Layout.add_to_parent(this, box);
    }

    update(dt)
    {
        for(let i = 0; i < this.physics_objs.length; ++i) {
            const obj = this.physics_objs[i];
            obj.update(dt);
        }
    }
} // class MenuScene


// ... add some bodies to the world
