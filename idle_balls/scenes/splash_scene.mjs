import { luna } from "../../../libs/ark_luna/luna/luna.mjs"
import { Game_Utils } from "../game_utils.mjs";


//
// Splash Scene
//
export class Splash_Scene
    extends luna.Base_Scene
{
    //--------------------------------------------------------------------------
    constructor()
    {
        super();
        luna.App.set_clear_color(luna.Utils.COLOR_WHITE);

        this.loading_complete = false;
        this.loading_count    = TEXTURES_TO_LOAD.length + 1; // +1 is for data....


        // Start to load
    }

    //--------------------------------------------------------------------------
    on_load()
    {
        super.on_load();

        const info = {
            resources_to_load: [],
            on_progress: ()=> {},
            on_error: ()=> {},
            on_load: ()=> { this.update_loading(); },
            on_complete: ()=> { this.start_end_animation(); }
        }

        luna.RES.load_resources_with_info(info);
    }

    //--------------------------------------------------------------------------
    update_loading()
    {
        --this.loading_count;
        const value = 1 - (this.loading_count / TEXTURES_TO_LOAD.length);

        console.log("Loading: ", this.loading_count)
    }

    //--------------------------------------------------------------------------
    start_end_animation()
    {
        this.on_scene_manager_splash_has_completed();
    }
}


//
// Loading Bar
//
class Loading_Bar
    extends PIXI.Container
{
    constructor()
    {
        super();

        this._completion = 0;
        this._text       = new luna.Base_TTFText(Game_Utils.SMALL_FONT_INFO, "Loading 0%");
        this.addChild(this._text);
    }

    set_completion(value)
    {
        this._text.text = luna.Str.concat(
            "Loading ", Math.trunc(value * 100), "%"
        );
    }
};
