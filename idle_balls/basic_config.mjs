import {Game_Utils} from "./game_utils.mjs";

//
// Basic Config
//
export class Basic_Config
{
    //--------------------------------------------------------------------------
    static get_default_config()
    {
        return {
            title:       Game_Utils.GAME_NAME,
            version:     Game_Utils.GAME_VERSION,
            target_fps:  60,
            design_size: {x: Game_Utils.DESIGN_WIDTH, y: Game_Utils.DESIGN_HEIGHT},
        }
    }
}
