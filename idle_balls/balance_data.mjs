export class Balance_Data
{
    static get_from_level(level)
    {
        return new Balance_Data(level);
    }

    constructor(level)
    {
        this.level = level;
        this.rows = 5;
        this.cols = 5;
        this.number_of_boxes = this.rows * this.cols;
        this.number_of_box_types = 2;
    }

    calculate_points(match_length)
    {
        return Math.pow(match_length-2, 2);
    }
}
