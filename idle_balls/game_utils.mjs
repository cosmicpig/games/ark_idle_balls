import { luna } from "../libs/ark_luna/luna/luna.mjs";
import { Nine_Slice_Button } from "./ui/nine_slice_button.mjs";

export class Game_Utils
{
    //
    // Balls
    //
    static BALL_RADIUS = 10;


    //
    // Game
    //
    static GAME_NAME    = "idle balls";
    static GAME_VERSION = "1.0.0";

    //
    // Design
    //
    static DESIGN_WIDTH  = 600;
    static DESIGN_HEIGHT = 800;


    //
    // Colors
    //

    //
    // Score
    //
    static SCORE_DIGITS_COUNT = 8;

    //
    // Animation
    //
    static ANIMATION_SPEED_MULTIPLIER = 1.0;

    //
    // Font
    //
    static LOADING_FONT_INFO = {
        size: 30,
        family: "ARCO"
    };

    static SMALL_FONT_INFO = {
        size: 25,
        family: "ARCO"
    };

    static MEDIUM_FONT_INFO = {
        size: 30,
        family: "ARCO"
    };

    static BIG_FONT_INFO = {
        size: 60,
        family: "ARCO"
    };

    //
    // Buttons
    //
    static BUTTON_BIG_WIDTH   = (Game_Utils.DESIGN_WIDTH * 0.5);
    static BUTTON_GAP         = 10;
    static BUTTON_SMALL_WIDTH = Game_Utils.BUTTON_BIG_WIDTH / 2 - Game_Utils.BUTTON_GAP * 0.5;
    static BUTTON_HEIGHT      = Game_Utils.BUTTON_BIG_WIDTH / 3 - Game_Utils.BUTTON_GAP;
    static BUTTON_BACK_WIDTH  = Game_Utils.BUTTON_HEIGHT;

    //
    // Nine Slice Settings
    //
    static NINE_SLICE_SETTINGS = {
        left_width:    3,
        top_height:    3,
        right_width:   3,
        bottom_height: 3
    };

    //
    // Size Settings
    //
    static BIG_BUTTON_SIZE_SETTINGS = {
        width:  Game_Utils.BUTTON_BIG_WIDTH,
        height: Game_Utils.BUTTON_HEIGHT
    };
    static SMALL_BUTTON_SIZE_SETTINGS = {
        width:  Game_Utils.BUTTON_SMALL_WIDTH,
        height: Game_Utils.BUTTON_HEIGHT
    };
    static BACK_BUTTON_SIZE_SETTINGS = {
        width:  Game_Utils.BUTTON_BACK_WIDTH,
        height: Game_Utils.BUTTON_HEIGHT
    };

    //
    // Textures Settings
    //
    static GREEN_TEXTURE_SETTINGS = {
        normal: RESOURCES_TEXTURES_BUTTONS_BOX_GREEN,
        press:  RESOURCES_TEXTURES_BUTTONS_BOX_GREEN_PRESSED
    };
    static ORANGE_TEXTURE_SETTINGS = {
        normal: RESOURCES_TEXTURES_BUTTONS_BOX_RED,
        press:  RESOURCES_TEXTURES_BUTTONS_BOX_RED_PRESSED
    };
    static YELLOW_TEXTURE_SETTINGS = {
        normal: RESOURCES_TEXTURES_BUTTONS_BOX_YELLOW,
        press:  RESOURCES_TEXTURES_BUTTONS_BOX_YELLOW_PRESSED
    };
    static BLUE_TEXTURE_SETTINGS = {
        normal: RESOURCES_TEXTURES_BUTTONS_BOX_BLUE,
        press:  RESOURCES_TEXTURES_BUTTONS_BOX_BLUE_PRESSED
    };

    static BOX_TEXTURES =  [
        RESOURCES_TEXTURES_BRICKS_4,
        RESOURCES_TEXTURES_BRICKS_2,
        RESOURCES_TEXTURES_BRICKS_3,
        RESOURCES_TEXTURES_BRICKS_1,
        RESOURCES_TEXTURES_BRICKS_5,
    ];

    static BOXES_TEXTURES_COUNT = Game_Utils.BOX_TEXTURES.length;


    //
    // Particles
    //
    static SCORE_TEXT_PARTICLE_DATA_NAME = "resources/particles/score.json";
    static SCORE_TEXT_PARTICLE_TEXTURES = [
        RESOURCES_TEXTURES_PARTICLES_EMOTE_HEARTS,
        RESOURCES_TEXTURES_PARTICLES_EMOTE_STAR ,
        RESOURCES_TEXTURES_PARTICLES_EMOTE_HEART,
        RESOURCES_TEXTURES_PARTICLES_EMOTE_STARS
    ];


    //
    // Ground Options
    //
    static GROUND_OPTIONS = {
        ground_texture_names : [
            RESOURCES_TEXTURES_SCENARIO_SLICE02_02, RESOURCES_TEXTURES_SCENARIO_SLICE03_03, RESOURCES_TEXTURES_SCENARIO_SLICE04_04,
            RESOURCES_TEXTURES_SCENARIO_SLICE33_33, RESOURCES_TEXTURES_SCENARIO_SLICE33_33, RESOURCES_TEXTURES_SCENARIO_SLICE33_33,
            RESOURCES_TEXTURES_SCENARIO_SLICE33_33, RESOURCES_TEXTURES_SCENARIO_SLICE33_33, RESOURCES_TEXTURES_SCENARIO_SLICE33_33,
        ],

    }

    //
    // Functions
    //
    //--------------------------------------------------------------------------
    static fill_digits(value, digits_count)
    {
        let value_str = value.toString();
        if(value_str.length < digits_count) {
            value_str = "0".repeat(digits_count - value_str.length) + value_str;
        }
        return value_str;
    }

    //--------------------------------------------------------------------------
    static create_back_button(callback)
    {
        const back_button = new Nine_Slice_Button(
            Game_Utils.ORANGE_TEXTURE_SETTINGS,
            Game_Utils.NINE_SLICE_SETTINGS,
            Game_Utils.BACK_BUTTON_SIZE_SETTINGS,
        );

        luna.Layout.set_anchor_to_center(back_button);

        back_button.scale.set(0.6);
        back_button.x = (back_button.width  * 0.5);
        back_button.y = (back_button.height * 0.5);
        back_button.on_pointer_down(()=> { callback() });
        back_button.add_icon(luna.RES.create_sprite_with_texture_name(RESOURCES_TEXTURES_BUTTONS_ICON_ARROW_LEFT));

        return back_button;
    }

    static get_box_texture_name(index)
    {
        return Game_Utils.BOX_TEXTURES[index];
    }
}
