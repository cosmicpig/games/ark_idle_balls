import { luna } from "../libs/ark_luna/luna/luna.mjs";

import { Basic_Config } from "./basic_config.mjs";
import { Game_Settings } from "./game_settings.mjs";
import { Game_Physics } from "./game_physics.mjs";

import { Splash_Scene }  from "./scenes/splash_scene.mjs";
import { Menu_Scene }    from "./scenes/menu_scene.mjs";
import { Credits_Scene } from "./scenes/credits_scene.mjs";
import { Game_Scene }    from "./scenes/game_scene.mjs";
import {GUI} from "../libs/ark_luna/luna/gui.mjs";


//
// Our beautiful hello message to curious minds ;D
//
luna.App.display_hello();


//
// App Callbacks
//
//------------------------------------------------------------------------------
luna.App.pre_init = ()=> {

}

//------------------------------------------------------------------------------
luna.App.pre_load = async ()=> {
    luna.RES.init();
    await luna.RES.load_resources_with_info({
        resources_to_load: [
            // RESOURCES_TEXTURES_LOGO,
            // "resources/fonts/ARCO.ttf"
        ]
    });
}

//------------------------------------------------------------------------------
luna.App.init = ()=> {
    // Settings.
    Game_Settings.init();

    // Editor Gui.
    luna.GUI.init();

    // Scenes.
    luna.Scene_Manager.init();
    luna.Scene_Manager.register_scene(
        Splash_Scene,
        Menu_Scene,
        Credits_Scene,
        Game_Scene,
    );

    luna.Scene_Manager.set_splash_scene (Splash_Scene);
    luna.Scene_Manager.set_default_scene(Menu_Scene);

    // Audio.
    luna.Audio_Manager.init();
    luna.Audio_Manager.preload_music();

    // Input.
    luna.Input_Manager.init();
    luna.Input_Manager.install_basic_mouse_handlers();

    // Physics
    const physics_options = {
        width: 800,
        height: 600,
        pixelRatio: 1,
        background: '#fafafa',
        wireframeBackground: '#222',
        hasBounds: false,
        enabled: true,
        wireframes: true,
        showSleeping: true,
        showDebug: false,
        showBroadphase: false,
        showBounds: false,
        showVelocity: false,
        showCollisions: true,
        showSeparations: false,
        showAxes: false,
        showPositions: false,
        showAngleIndicator: false,
        showIds: false,
        showShadows: false,
        showVertexNumbers: false,
        showConvexHulls: false,
        showInternalEdges: false,
        showMousePosition: false
    };
    Game_Physics.init(physics_options);
    // GUI.make(Game_Physics.engine)
    GUI.make(Game_Physics.render)

    // Start up ;D
    const params = luna.Utils.get_url_params();
    luna.Scene_Manager.run_with(params);
}

//------------------------------------------------------------------------------
luna.App.loop = ()=> {

}

//------------------------------------------------------------------------------
luna.App.resize = ()=> {

}


//
// Let the fun begins ;DD
//
luna.App.set_config(Basic_Config.get_default_config());
await luna.App.start();
