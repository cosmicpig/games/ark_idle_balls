import { luna }              from "../../libs/ark_luna/luna/luna.mjs";
import { Game_Settings }     from "./game_settings.mjs";
import { Game_Utils    }     from "./game_utils.mjs";

//
// Matter aliases
//
const MEngine = Matter.Engine;
const MWorld  = Matter.World;
const MBodies = Matter.Bodies;
const MRender = Matter.Render;


//
// Game Physics
//
export class Game_Physics
{
    static engine;
    static world;
    static render;

    static init(options)
    {
        Game_Physics.engine = MEngine.create();
        Game_Physics.world  = Game_Physics.engine.world;
        Game_Physics.render = MRender.create({
            element: document.body,
            engine:  Game_Physics.engine,
            options: options
        });

        MEngine.run(Game_Physics.engine);
        MRender.run(Game_Physics.render);
    }
}

//
// Physics_Object
//
export class Physics_Object
    extends PIXI.Container
{
    constructor(x, y)
    {
        super();

        this.body     = null;
        this.graphics = null;
        this.x        = x;
        this.y        = y;

    }

    update(dt)
    {
        this.x = this.body.position.x;
        this.y = this.body.position.y;
    }
}

//
// Physics Brick
//
export class Physics_Brick
    extends Physics_Object
{
    constructor(x, y)
    {
        super(x, y);

        this._setup();
    }

    _setup()
    {
        // Init Physics.
        const options = {
            friction:    0,
            frictionAir: 0,
            rot:         0,
            restitution: 1,
            angle:       0,
            isStatic:    true
        }

        this.body = MBodies.rectangle(this.x, this.y, 100, 100, options);
        MWorld.add(Game_Physics.world, this.body);

        // Init Graphics.
        this.graphics = new PIXI.Graphics();

        this.graphics.beginFill(0xCC3333);
        this.graphics.lineStyle(0, 0x993333);
        this.graphics.drawRect(-50, -50, 100, 100);
        this.graphics.endFill();

        luna.Layout.add_to_parent(this, this.graphics);
    }

}

//
// Physics Balls
//
export class Physics_Ball
    extends Physics_Object
{
    constructor(x, y)
    {
        super(x, y);
        this.radius = Game_Utils.BALL_RADIUS;

        this._setup();
    }

    _setup()
    {
        // Init Physics.
        const options = {
            friction:    0,
            frictionAir: 0,
            rot:         0,
            restitution: 1,
            angle:       0,
        }

        this.body = MBodies.circle(this.x, this.y, this.radius, options);
        MWorld.add(Game_Physics.world, this.body);

        // Init Graphics.
        this.graphics = new PIXI.Graphics();

        this.graphics.beginFill(0xCC3333);
        this.graphics.lineStyle(0, 0x993333);
        this.graphics.drawCircle(-this.radius * 0.5, -this.radius * 0.5, this.radius);
        this.graphics.endFill();

        luna.Layout.add_to_parent(this, this.graphics);
    }
}
