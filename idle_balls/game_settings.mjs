//------------------------------------------------------------------------------
export class Game_Settings
{

    static KEY_HAS_SCORE  = "key_has_score";
    static KEY_LAST_SCORE = "key_last_score";
    static KEY_BEST_SCORE = "key_best_score";

    static KEY_SOUND_ENABLED = "key_sound_enabled";

    static _storage = null;

    static init()
    {
        // @notice(stdmatt): Maybe in future we might want to put that in
        // the indexed db or some other way...
        Game_Settings._storage = localStorage;
        if(!Game_Settings._storage) {
            console.log("Can't store data on this browser...");
        }
    }

    static set(key, value)
    {
        if(Game_Settings._storage) {
            Game_Settings._storage.setItem(key, value);
        }
    }

    static get(key, default_value)
    {
        if(Game_Settings._storage) {
            const value = Game_Settings._storage.getItem(key);
            if(value) {
                switch(typeof(default_value)) {
                    case "number":  return Number(value);   break;
                    case "boolean": return value == "true"; break;
                }
            }
        }

        return default_value;
    }
};
